Garden
======

Garden is Lily's package manager. It's not as complete as package managers of
other languages. Even so, it assists in the development of Lily projects by
automating the build and install of packages.

Garden's catalog is in this repository:

https://gitlab.com/FascinatedBox/lily-catalog

Garden works by first cloning data into a local cache, then cloning from that
cache into the current directory.

On Windows, the cache is at `C:\Users\<username>\.lily\garden\`.

On Linux, the cache is at `~/.lily/garden/`.

Garden currently understands the following commands:

### install <name>...

Installs `name` to `packages/<repository>`. If `name` includes a version
(ex: `FascinatedBox/lily-dis@v1.2.0`), then that specific version is installed.
If a version is not named (ex: `FascinatedBox/lily-dis`), then the most recent
version is selected. This is not case-sensitive, so `FaScInAtEdBoX/lIlY-dIs`
will also work to install the latest version of that package.

If the catalog has not been downloaded yet, this will attempt to download the
catalog first.

### update

Downloads the initial catalog, or updates the existing catalog.
